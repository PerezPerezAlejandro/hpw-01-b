
var Grupo = function (_clave, _nombre, _docente, _materias, _alumnos){
    return {
        "clave": _clave,
        "nombre": _nombre,
        "docente": _docente,
        "materias": _materias,
        "alumnos": _alumnos
    };
    };
    
var grupo1= Grupo("G01",'A');
var grupo2= Grupo("G02",'B');
var grupo3= Grupo("G03",'C');

/*********************************************************************/

var Docente= function (_clave, _nombre, _apellidos, _grado_academico){
    return {
        "clave": _clave,
        "nombre": _nombre,
        "apellidos": _apellidos,
        "grado_academico": _grado_academico
    };
};

var docente1= Docente("D01", "Antonio", "Hernandez Blas", "Ingeniero en sistemas computacionales");
var docente2= Docente("D02", "Anayansi", "Cabrera Ruiz", "Ingeniero en sistemas computacionales");
var docente3= Docente("D03", "Luis", "Hernandez Mendez", "Licenciado en informatica");
var docente4= Docente("D04", "Pedro", "Cruz Jimenez", "Licenciado en informatica");
var docente5= Docente("D05", "Ana", "Jimenez Perez", "Ingeniero en sistemas computacionales");

/************************************************************************/

function asignarDocenteGrupo(g,d){
    if(!g.docente){
        g.docente=d;
        return{
            "exito": true,
            "mensaje": "El docente " + d.nombre + " " + d.apellidos + " ha sido asignado al grupo "+ g.nombre
        };
    }
       return{
           "exito": false,
           "mensaje": "El grupo "+ g.nombre + " ya tiene asignado un docente"
       };
}

asignarDocenteGrupo(grupo1, docente1);
asignarDocenteGrupo(grupo2, docente2);
asignarDocenteGrupo(grupo3, docente3);

/************************************************************************/

function existeDocenteGrupo (g,cd){
    if(g.docente && g.docente.clave === cd){
        return{
            "exito": true,
            "mensaje": "El docente " + g.docente.nombre + " " + g.docente.apellidos + " esta asignado al grupo " + g.nombre
        };
    }
        return {
            "exito": false,
            "mensaje": "el docente no esta signado al grupo " + g.nombre
        };
}

// existeDocenteGrupo(grupo1, "D01");

/**********************************************************************/

function borrarDocenteGrupo(g){
    if(!g.docente){
        return{
            "exito": false,
            "mensaje": "El grupo " + g.nombre + " no tiene asignado un docente"
        };
    }   
        var nomDocente= g.docente.nombre + " " + g.docente.apellidos;
        g.docente= undefined;
        return{
            "exito": true,
            "mensaje": "El docente " + nomDocente + " ha sido borrado del grupo "+ g.nombre
        };
}

borrarDocenteGrupo(grupo2);

/********************************************************************/

var Materia= function (_clave, _nombre){
    return{
        "clave": _clave,
        "nombre": _nombre
    };
}

var materia1= Materia('M01', 'Programacion');
var materia2= Materia('M02', 'Ecuaciones diferenciales');
var materia3= Materia('M03', 'Geografia');

/*********************************************************************/

function existeMateriaEnGrupo(g,cm){  
    if((!g.materias) && (g.materias.length===0)){
        return false;
    }
    for(var i=0; i<g.materias.length; i++){
        if(g.materias[i].clave===cm){
            return true;
        }
    }   
        return false;
}

// existeMateriaEnGrupo(grupo1, "M02");

/************************************************************************/

function agregarMateriaGrupo(g,m){
    if(!g.materias){
        g.materias= [];
    }
    
    if(!existeMateriaEnGrupo(g, m.clave)){
        g.materias.push(m);
        return {
            "exito": true,
            "Mensaje": "La materia " + m.nombre + " ha sido agregada al grupo " + g.nombre + " correctamente"
        };
    }
    return {
        "exito": false,
        "Mensaje": "No se agrego la materia " + m.nombre + " porque ya esta agregada al grupo " + g.nombre
    };
}

agregarMateriaGrupo(grupo1, materia1);
agregarMateriaGrupo(grupo1, materia2);
agregarMateriaGrupo(grupo1, materia3);

/***Busca el indice de la materia especificada en el grupo especificado***/

function buscarMateriaEnGrupo(g, cm){
    if((!g.materias) && (g.materias.length===0)){
        return -1;
    }
    for(var i=0; i<g.materias.length; i++){
        if(g.materias[i].clave===cm){
            return i;
        }
    }
    return -1;
}  

// buscarMateriaEnGrupo(grupo1, "M03");

/*****************************************************************/

function borrarMateriaGrupo(g, m){
    if(!existeMateriaEnGrupo(g, m.clave)){
        return{
            "exito": false,
            "Mensaje": "No se pudo eliminar la materia " + m.nombre + " porque no esta asignada el grupo " + g.nombre
        };
    }     if(buscarMateriaEnGrupo(g, m.clave)>=0){
              var indice= buscarMateriaEnGrupo(g, m.clave);
              var nomMateria= g.materias[indice].nombre;
              g.materias.splice(indice,1);
              return{
                  "exito": true,
                  "Mensaje": "La materia " + nomMateria + " ha sido eliminada del grupo " + g.nombre
              };
    }
}

borrarMateriaGrupo(grupo1, materia1);

/******************************************************************/

var Alumno= function(_clave, _nombre, _apellidos, _calificaciones){
    return{
        "clave": _clave,
        "nombre": _nombre,
        "apellidos": _apellidos,
        "calificaciones": _calificaciones
    };
};

var alumno1= Alumno("A01", "Alejandro", "Perez Perez");
var alumno2= Alumno("A02", "Erika", "Cabrera Cordero");
var alumno3= Alumno("A03", "Fernando", "Monterrubio Jimenez");
var alumno4= Alumno("A04", "Paola", "Jimenez Hernandez");
var alumno5= Alumno("A05", "Itzel", "Gomez Cruz");

/***Verifica si existe el alumno en el grupo especificado***/

function existeAlumnoEnGrupo(g, ca){
    if((!g.alumnos) && (g.alumnos.length===0)){
        return false;
    }
    for(var i=0; i<g.alumnos.length; i++){
        if(g.alumnos[i].clave===ca){
            return true;
        }
    }
    return false;
}

/****Agrega un alumno a un grupo si no existe****/

function agregarAlumnoGrupo(g,a){
    if(!g.alumnos){
        g.alumnos= [];
    }
    
    if(!existeAlumnoEnGrupo(g, a.clave)){
        g.alumnos.push(a);
        return{
            "exito": true,
            "Mensaje": "El alumno " + a.nombre + " " + a.apellidos + " ha sido agregado al grupo " + g.nombre
        };
    }
    return{
        "exito": false,
        "Mensaje": "No se agrego el alumno " + a.nombre + " " + a.apellidos + " porque ya esta en el grupo " + g.nombre
    };
}

agregarAlumnoGrupo(grupo1, alumno1);
agregarAlumnoGrupo(grupo1, alumno2);
agregarAlumnoGrupo(grupo1, alumno3);
agregarAlumnoGrupo(grupo1, alumno4);
agregarAlumnoGrupo(grupo1, alumno5);

/********************************************************************/

function buscarAlumnoEnGrupo(g, ca){
    if((!g.alumnos) && (g.alumnos.length===0)){
        return -1;
    }
    for(var i=0; i<g.alumnos.length; i++){
        if(g.alumnos[i].clave===ca){
            return i;
        }
    }
    return -1;
}

// buscarAlumnoEnGrupo(grupo1,"A01");

/******************************************************************/

function borrarAlumnoGrupo(g, a){
    if(!existeAlumnoEnGrupo(g, a.clave)){
        return{
            "exito": false,
            "Mensaje": "No se elimino el alumno " +a.nombre+ " " + a.apellidos + " porque no esta en el grupo " + g.nombre
         };
    }
    if(buscarAlumnoEnGrupo(g, a.clave)>=0){
        var indice= buscarAlumnoEnGrupo(g, a.clave);
        var nomAlumno= g.alumnos[indice].nombre + " " + g.alumnos[indice].apellidos;
        g.alumnos.splice(indice,1);
        return{
            "exito": true,
            "Mensaje": "El alumno " + nomAlumno + " ha sido eliminado del grupo " + g.nombre
        };
    }
}

borrarAlumnoGrupo(grupo1, alumno3);     

/** Lista las materias de un grupo especificado y muestra un arreglo de esas materias **/

function verMateriasDeGrupo(g){
    if(!g.materias){
        return{
            "exito": false,
            "Mensaje": "El grupo " + g.nombre + " aun no tiene materias asignadas"
        };
    }
    objMaterias=[];
    for(var i=0; i<g.materias.length; i++){
            objMaterias.push(g.materias[i]);
            //objMaterias.push((g.materias[i].nombre));  //Lista solo el nombre de las materias
            console.log(g.materias[i].nombre);  //Imprime en consola las materias del grupo
    }
    return objMaterias;
}

verMateriasDeGrupo(grupo1);

/**Regresa un arreglo de todos los alumnos del grupo especificado ademas lista los alumnos**/

function verAlumnosDeGrupo(g){
    if(!g.alumnos){
        return{
            "exito": false,
            "Mensaje": "El grupo " + g.nombre + " aun no tiene alumnos agregados"
        };
    }
    objAlumnos= [];
    for(var i=0; i<g.alumnos.length; i++){
        var nomAlum= g.alumnos[i].nombre + " " + g.alumnos[i].apellidos;
        objAlumnos.push(nomAlum);      //Se muestran los nombres de los alumnos
        //objAlumnos.push(g.alumnos[i]);        //muestra el arreglo con datos completos de los alumnos
        console.log(nomAlum);     // Imprime los nombres de los alumnos en consola
    }
    return objAlumnos;
}

verAlumnosDeGrupo(grupo1);

/** Muestra el docente del grupo especificado **/

function verDocenteDeGrupo(g){
    if(!g.docente){
        return{
            "exito": false,
            "mensaje": "El grupo " + g.nombre + " aun no tiene un docente asignado"
        };
    }
    console.log(g.docente.nombre + " " + g.docente.apellidos);  //Muestra en consola el nombre completo del docente
    docente=[];
    docente.push(g.docente);
    return docente;
}

verDocenteDeGrupo(grupo1);

//** Funcion para comprobar si existe asignada ya una calificacion de una materia **///

function existeCalificacionDeAlumno(alum, clave_mat){
    if(alum.calificaciones.length===0){
        return false;
    }
    for(var i=0; i<alum.calificaciones.length; i++){
    if(alum.calificaciones[i].clave_materia===clave_mat){
        return true;
    }
    }
    return false;
}

/*Asignar calificacion a un alumno perteneciente a cierto grupo y que lleve cierta materia que exista en el grupo*/

function asignarCalificacionAlumno(_alumno, _grupo, _materia, _calificacion){
    if( (_grupo.alumnos) && existeAlumnoEnGrupo(_grupo, _alumno.clave)){
        if((_grupo.materias) && existeMateriaEnGrupo(_grupo, _materia.clave)){
            if(!_alumno.calificaciones){
               _alumno.calificaciones=[];
               }
            if(!existeCalificacionDeAlumno(_alumno,_materia.clave)){
            objCalificacion={"clave_materia": _materia.clave, "calificacion": _calificacion};
            _alumno.calificaciones.push(objCalificacion);
            return{
                "exito": true,
                "Mensaje": "Se ha asignado correctamente la calificacion al alumno "+ _alumno.nombre+ " " + _alumno.apellidos+ " de la materia " + _materia.nombre+ " perteneciente al grupo " + _grupo.nombre
            };
            }
        }
        return{
            "exito": false,
            "Mensaje": "No se agrego la calificacion porque esa materia no esta asignada al grupo o ya se ha asignado una calificacion"
        };
    }
        return {
            "exito": false,
            "Mensaje": "No se agrego la calificacion poque el alumno no pertenece al grupo"
            };
}

asignarCalificacionAlumno(alumno1, grupo1, materia1, 10);

/** Listar las calificaciones de un alumno  **/

function verCalificacionesAlumno(a){
    if(!a.calificaciones){
        return{
            "exito": false,
            "Mensaje": "No se tienen registradas calificaciones del alumno"
        };
    }
    objCalif=[];
    for(var i=0; i<a.calificaciones.length; i++){
        console.log(a.calificaciones[i].clave_materia + " : " + a.calificaciones[i].calificacion);
        objCalif.push(a.calificaciones[i]);
    }
    return objCalif;
}

verCalificacionesAlumno(alumno1);

//** Busca el indice de la posicion de una calificacion de materia especifica de un alumno **//

function buscarCalificacion(a, clave_mat){
    if(!a.calificaciones){
       return -1;
    }
    for(var i=0; i<a.calificaciones.length; i++){
        if(a.calificaciones[i].clave_materia===clave_mat){
            return i;
        }
    }
    return -1;
}

/** Actualiza una calificacion de un alumno dya existente **/

function actualizarCalificacionesAlumno(alum, mat, nueva_calificacion){
    if(buscarCalificacion(alum, mat.clave)>=0){
        var indice= buscarCalificacion(alum, mat.clave);
        alum.calificaciones[indice].calificacion= nueva_calificacion;
        return{
            "exito": true,
            "mensaje": "Se ha actualizado correctamente la calificacion de la materia de "+mat.nombre+" para el alumno "+ alum.nombre+" "+ alum.apellidos
        };
        
    }
    return{
        "exito": false,
        "Mensaje": "No se tienen registros de la materia a la cual se quiere actualizar la calificacion"
    };
}

actualizarCalificacionesAlumno(alumno1, materia1, 5);

/** Eliminar la calificacion del alumno de una materia especificada **/

function borrarCalificacionAlumno(a, mat){
    if(buscarCalificacion(a, mat.clave)>=0){
        var indice=buscarCalificacion(a, mat.clave);
        a.calificaciones.splice(indice, 1);
        return{
            "exito": true,
            "mensaje": "Se ha eliminado correctamente la calificacion de la materia "+mat.nombre+ " del alumno "+a.nombre+" "+ a.apellidos
        };
    }
    return{
        "exito": false,
        "mensaje": "No se pudo eliminar la calificacion ya que no esta registrada para el alumno"
    };
}

 borrarCalificacionAlumno(alumno1, materia1);


